Feature: Testing different approaches of writing scenarios in BDD

  Scenario: Unsuccessful logging in
    Given user is populating login form with john.doe@email.com and password combination
    When user clicks Ok button
    Then LOGIN_WRONG_COMBINATION error message appears


  Scenario: Create new account
    Given user is navigating to Register page
    When user populates register form with data
      | email        | jane.doe@gmail.com |
      | new password | pass123            |
      | password     | pass123            |
      | first name   | Jane               |
      | last name    | Doe                |
  Then REGISTER_NO_MOBILE error message appears

  Scenario Outline: Unsuccessful logging in many many combinations
    Given user is populating login form with <login> and <password> combination
    When user clicks Ok button
    Then <keyError> error message appears

    Examples:
      | login                    | password    | keyError                |
      | jane.doe@email.com       |             | LOGIN_EMPTY_PASSWORD    |
      | bruce.williams@email.com | 123password | LOGIN_WRONG_COMBINATION |
      |                          | 123         | LOGIN_INVALID_EMAIL     |


