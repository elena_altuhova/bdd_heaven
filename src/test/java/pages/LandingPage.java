package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LandingPage extends DefaultPage {

    public LandingPage(WebDriver driver) {
        super(driver);
        driver.manage().window().maximize();
        driver.navigate().to("http://www.airmoldova.md");
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = ".//a[text()='Login']")
    private WebElement login;

    @FindBy(xpath = "//*[@id='login']")
    private WebElement rezervariLogin;

    @FindBy(xpath = "//*[@id='emailLogin']")
    private WebElement email;

    @FindBy(xpath = "//*[@id='passwordLogin']")
    private WebElement password;

    @FindBy(xpath = "//*[@id='login-box']/form/button")
    private WebElement loginButton;

    @FindBy(xpath = "//*[@id='login-box']/form/div[3]/a[1]")
    private WebElement createAccount;

    public void clickLogin(){
        login.click();
    }

    public void clickRezervariLogin(){
        rezervariLogin.click();
    }

    public void setEmailLogin(String emailLogin){
        email.sendKeys(emailLogin);
    }

    public void setPassword(String passwordLogin){
        password.sendKeys(passwordLogin);
    }

    public void clickOk(){
        loginButton.click();
    }

    public void clickCreateAccount(){
        createAccount.click();
    }
}
