package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DefaultPage {
    protected static WebDriver driver;

    public DefaultPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    @FindBy(xpath = "//div[contains(@class, 'system-message error')]")
    private WebElement errorMessage;

    public String getErrorMessage(){
        return errorMessage.getText();
    }
}
