package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegisterPage extends DefaultPage {

    public RegisterPage(WebDriver driver) {
        super(driver);
        driver.manage().window().maximize();
        driver.navigate().to("https://www.airmoldova.md/index/register");
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@id='email']")
    private WebElement email;

    @FindBy(xpath = "//*[@id='newPassword']")
    private WebElement newPassword;

    @FindBy(xpath = "//*[@id='repeatNewPassword']")
    private WebElement password;

    @FindBy(xpath = "//*[@id='firstName']")
    private WebElement firstName;

    @FindBy(xpath = "//*[@id='lastName']")
    private WebElement lastName;

    @FindBy(xpath = "/html/body/div[1]/div/div/div[2]/main/form/footer/button")
    private WebElement submit;



    public void clickSubmit() {
        submit.click();
    }

    public void setEmail(String emailLogin) {
        email.sendKeys(emailLogin);
    }

    public void setNewPassword(String newPasswrd) {
        newPassword.sendKeys(newPasswrd);
    }

    public void setPassword(String passwrd) {
        password.sendKeys(passwrd);
    }

    public void setFirstName(String fname) {
        firstName.sendKeys(fname);
    }

    public void setLastName(String lname) {
        lastName.sendKeys(lname);
    }

}
