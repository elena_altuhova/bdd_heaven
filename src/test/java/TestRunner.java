import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(format = {"pretty", "html:target/cucumber"},
        glue = {"stepdefs"},
        //tags = {},
        features = {"src/test/resources/"})
public class TestRunner {

}
