package messages;

public enum ErrMsgs {
    LOGIN_EMPTY_PASSWORD ("PLEASE ENTER YOUR PASSWORD AND TRY AGAIN."),
    LOGIN_WRONG_COMBINATION ("THE ENTERED COMBINATION OF E-EMAIL AND PASSWORD IS NOT CORRECT, PLEASE TRY AGAIN. USER NOT EXIST WITH THIS VALUES."),
    LOGIN_INVALID_EMAIL("THE E-MAIL ADDRESS YOU ENTERED IS INVALID."),
    REGISTER_NO_MOBILE("VĂ RUGĂM SĂ INTRODUCEŢI CEL PUŢIN NUMĂRUL DE TELEFON MOBIL.");

    private String errorMessage;

    ErrMsgs(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage(){
        return errorMessage;
    }
}
