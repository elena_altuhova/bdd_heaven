package stepdefs;

import cucumber.api.java.en.Then;
import messages.ErrMsgs;
import org.junit.Assert;
import pages.DefaultPage;

public class GenericStepDefs extends AbstractStepDefs{

    DefaultPage defaultPage;

    @Then("^(.*) error message appears$")
    public void errorAppears(String errorMessage) {
        defaultPage = new DefaultPage(driver);
        Assert.assertEquals(defaultPage.getErrorMessage(), ErrMsgs.valueOf(errorMessage).getErrorMessage());
    }
}
