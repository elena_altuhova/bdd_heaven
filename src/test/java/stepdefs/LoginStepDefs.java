package stepdefs;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import messages.ErrMsgs;
import org.junit.Assert;
import pages.LandingPage;

public class LoginStepDefs extends AbstractStepDefs {

    LandingPage landingPage;

    @Given("^user is populating login form with (.*) and (.*) combination$")
    public void unsuccessfulLogin(String username, String password) {
        //TODO: populate a Map with pages and their names then create an object based on name
        getDriver();
        landingPage = new LandingPage(driver);
        landingPage.clickLogin();
        landingPage.clickRezervariLogin();
        landingPage.setEmailLogin(username);
        landingPage.setPassword(password);
    }

    @When("^user clicks (Ok|Close) button$")
    public void userClicksButton(String buttonName) {
        landingPage.clickOk();
    }
}
