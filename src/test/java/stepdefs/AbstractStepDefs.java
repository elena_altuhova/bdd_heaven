package stepdefs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public abstract class AbstractStepDefs {
    protected static WebDriver driver;

    protected WebDriver getDriver() {
        if ((driver == null) || (((RemoteWebDriver) driver).getSessionId() == null)) {
            System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
            driver = new ChromeDriver();
        }
        return driver;
    }

}

