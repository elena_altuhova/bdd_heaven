package stepdefs;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import messages.ErrMsgs;
import org.junit.Assert;
import pages.RegisterPage;

import java.util.List;

public class RegisterStepDefs extends AbstractStepDefs {

    RegisterPage registerPage;

    @Given("^user is navigating to Register page$")
    public void userIsNavigatingToRegisterPage() {
        getDriver();
        registerPage = new RegisterPage(driver);
    }

    @When("^user populates register form with data$")
    public void userPopulatesRegisterFormWithData(DataTable dataTable) {
        List<List<String>> data = dataTable.raw();

        registerPage.setEmail(data.get(0).get(1));
        registerPage.setNewPassword(data.get(1).get(1));
        registerPage.setPassword(data.get(2).get(1));
        registerPage.setFirstName(data.get(3).get(1));
        registerPage.setLastName(data.get(4).get(1));

        registerPage.clickSubmit();
    }

    protected String getErrorMessage(){
        return registerPage.getErrorMessage();
    }
}
